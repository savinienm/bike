<?php

function db_connect(){
    try
    {
        $connexion = new PDO('mysql:host=' . DB_SERVER . ';dbname=' . DB_NAME . ';charset=utf8', DB_USER, DB_PASSWORD);
    }
    catch (Exception $e)
    {
        die('Erreur : ' . $e->getMessage());
    }
    return $connexion;
}

function confirm_db_connection($connexion){
    if($connexion->connect_errno){
        $message = "Database connexion failed: ";
        $message .= $connexion->connect_error;
        $message .= " (" . $connexion->connect_errno . ")";
        exit($message);
    }
}
CREATE DATABASE IF NOT EXISTS db_bike;

CREATE TABLE IF NOT EXISTS bikes
(
    id INTEGER PRIMARY KEY NOT NULL,
    marque VARCHAR(255) NOT NULL,
    model VARCHAR(255) NOT NULL,
    annee INT(4) NOT NULL,
    category VARCHAR(255) NOT NULL,
    genre VARCHAR(255) NOT NULL,
    couleur VARCHAR(255) NOT NULL,
    poids DECIMAL(10,0) NOT NULL,
    prix DECIMAL(10,0) NOT NULL,
    etat VARCHAR(255) NOT NULL
);

INSERT INTO bikes (marque,model,annee,category,genre,couleur,poids,prix,etat) VALUES (
    'decathlon', 
    'rockrider',
    '2021',
    'VTT',
    'homme',
    'vert',
    '12',
    '1200',
    'neuf'
    );
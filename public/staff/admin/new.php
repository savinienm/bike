<?php require_once('../../../private/initialize.php');?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="new.php" method="post">
        <label for="marque">Marque :</label><input type="text" name="marque" id="marque" value="">
        <label for="modele">Modèle :</label><input type="text" name="modele" id="modele" value="">
        <label for="annee">Année :</label><input type="number" name="annee" id="annee" value="">
        <label for="category">Catégorie :</label><input type="text" name="category" id="category" value="">
        <label for="genre">Genre :</label><input type="text" name="genre" id="genre" value="">
        <label for="couleur">Couleur :</label><input type="text" name="couleur" id="couleur" value="">
        <label for="poids">Poids :</label><input type="number" name="poids" id="poids" value="">
        <label for="etat">État :</label><input type="text" name="etat" id="etat" value="">
        <label for="prix">Prix :</label><input type="number" name="prix" id="prix" value="">
        <input type="submit" value="Enregistrer">
    </form>
</body>
</html>

<?php
if(isset($_POST)) {
    if(!empty($_POST)) {
        $marque = verif_input($_POST['marque']);
        $modele = verif_input($_POST['modele']);
        $annee = verif_input($_POST['annee']);
        $category = verif_input($_POST['category']);
        $genre = verif_input($_POST['genre']);
        $couleur = verif_input($_POST['couleur']);
        $poids = verif_input($_POST['poids']);
        $etat = verif_input($_POST['etat']);
        $prix = verif_input($_POST['prix']);

        $new_bike =$database->prepare("INSERT INTO bikes (marque, model, annee, category, genre, couleur, poids, prix, etat) VALUES (:ValMarque, :ValModele, :ValAnnee, :ValCategory, :ValGenre, :ValCouleur, :ValPoids, :ValPrix, :ValEtat)");
        $new_bike->execute(array(':ValMarque' => $marque, ':ValModele' => $modele, ':ValAnnee' => $annee, ':ValCategory' => $category, ':ValGenre' => $genre, ':ValCouleur' => $couleur, ':ValPoids' => $poids,':ValPrix' => $prix, ':ValEtat' => $etat));
    } else {
            print('Il faut remplir tous les champs !');
        }
} else {
    print('Rends-moi mes codes !');
}